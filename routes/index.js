const express = require('express');
const router = express.Router();
const session = require('express-session');
const { body, validationResult } = require('express-validator');
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const sanitizeHtml = require('sanitize-html');
const helmet = require('helmet');
const helmetCSP = require('helmet-csp');
const xss = require('xss-clean');
const hpp = require('hpp');
const axios = require('axios');
const sqlite3 = require('sqlite3').verbose();

const db = new sqlite3.Database('./database/mydatabase.db'); //Relative path

// Create a CSRF protection middleware
// const csrf = require('csurf')({ cookie: true });
// router.use(csrf);

// Configure express-session middleware
const secretKey = crypto.randomBytes(32).toString('hex');
console.log('Secret Key:', secretKey);
router.use(
    session({
      secret: secretKey,
      resave: false,
      saveUninitialized: false,
      cookie: { maxAge: 60000 }
    })
)

// Helmet middleware for security headers
router.use(helmet());

// Helmet Content Security Policy (CSP) middleware
router.use(helmetCSP());

// XSS Clean middleware to sanitize user inputs
router.use(xss());

// HPP (HTTP Parameter Pollution) protection middleware
router.use(hpp());

// Define a route for the signup page (GET request)
router.get('/signup', (req, res) => {
  res.render('signup');
});

// Define a route to handle the signup form submission (POST request)
router.post('/signup', [
  // Validate user input from the request body
  body('username').notEmpty().trim().escape(),
  body('email').isEmail().normalizeEmail(),
  body('password').isLength({ min: 5 }).escape(),
], async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  // Destructure user input from the request body
  const { username, email, password } = req.body;

  // Sanitize user inputs to remove any potentially harmful HTML
  const sanitizedUsername = sanitizeHtml(username);
  const sanitizedEmail = sanitizeHtml(email);

  // Check if the email already exists in the database
  await db.get('SELECT email FROM users WHERE email = ?', [sanitizedEmail], async (err, existingEmail) => {
    if (err) {
      console.error(err);
      return res.status(500).json({ message: 'Internal server error' });
    }

    // Check if the username already exist in the database
    await db.get('SELECT username FROM users WHERE username = ?', [sanitizedUsername], async (err, existingUsername) => {
      if (err) {
        console.error(err);
        return res.status(500).json({ message: 'Internal server error' });
      }

      // If the email already exists, return an error response
      if (existingEmail) {
        return res.status(400).json({ message: 'Email already exists' });
      }

      // If the username already exists, return an error response
      if (existingUsername) {
        return res.status(400).json({ message: 'Username already exists' });
      }

      try {

        // Hash the password before storing it in the database
        const hashedPassword = await bcrypt.hash(password, 10);

        // Insert a new user into the users table
        await db.run('INSERT INTO users (username, email, password) VALUES (?, ?, ?)', [
          sanitizedUsername,
          sanitizedEmail,
          hashedPassword,
        ], function (err) {
          if (err) {
            console.error(err);
            return res.status(500).json({ message: 'Internal server error' });
          }
          req.session.user = { username: sanitizedUsername, email: sanitizedEmail };
          // Redirect to a success page (e.g., login) after user registration
          res.redirect('/login');
        });
      } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Internal server error' });
      }
    });
  });
});

// Define a route for the login page (GET request)
router.get('/login', (req, res) => {
  const user = req.session.user; // Retrieve user data from the session

  if (user) {
    // If the user is already logged in, redirect to a different page or perform any action you need
    return res.render('login',  { displayMessage: 'already logged in, your username is:', displayMessage2: user.username });
  }
  res.render('login', { displayMessage: undefined , displayMessage2: undefined });
});

// Define a route to handle the login form submission (POST request)
router.post('/login', [
  // Validate user input from the request body
  body('username').notEmpty().trim().escape(),
  body('password').notEmpty().trim(),
], async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  const { username, password } = req.body;

  // Fetch the user from the database by username
  const user = await new Promise((resolve, reject) => {
    db.get('SELECT id, username, password FROM users WHERE username = ?', [username], (err, row) => {
      if (err) {
        return reject(err);
      }
      resolve(row);
    });
  });

  if (!user) {
    // Invalid username
    return res.render('login', { displayMessage: 'fail', displayMessage2: undefined });
  }

  const passwordMatch = await bcrypt.compare(String(password), String(user.password));

  if (!passwordMatch) {
    // Invalid password
    return res.render('login', { displayMessage: 'fail', displayMessage2: undefined });
  }

  // Log in the user
  req.session.user = user;
  res.render('login', { displayMessage: 'success, reopen login page in a new tab to test saved session', displayMessage2: undefined });
});

/* Define a route handler that renders the 'template.ejs' template
router.get('/template', (req, res) => {
  res.render('template');
});

// Define an Express route handler for the form submission
router.post('/createTable', (req, res) => {
  const { username, tableName, columns } = req.body;

  if (!username || !tableName || !columns || !Array.isArray(columns)) {
    return res.status(400).json({ error: 'Invalid request data' });
  }

  // Generate a table name based on the user's username and an index (for uniqueness)
  const tableIndex = Math.floor(Math.random() * 1000); // Temporary index method for testing
  const generatedTableName = `${username}_${tableIndex}`;

  // Construct the CREATE TABLE SQL statement
  const createTableSQL = `CREATE TABLE ${generatedTableName} (${columns.map(col => `${col.name} ${col.type}`).join(', ')})`;

  // Execute the CREATE TABLE statement
  db.run(createTableSQL, err => {
    if (err) {
      return res.status(500).json({ error: 'Error creating table' });
    }

    res.status(201).json({ message: 'Table created successfully', tableName: generatedTableName });
  });
});

// Define an Express route handler for sending table data
router.get('/data/:tableName', async (req, res) => {
  const { tableName } = req.params;

  if (!tableName) {
    return res.status(400).json({ error: 'Table name is required' });
  }

  // Construct the SQL SELECT statement to fetch table data
  const selectTableSQL = `SELECT * FROM ${tableName}`;

  // Step 1: Fetch table columns and their names
  await db.all(`PRAGMA table_info(${tableName})`, async (err, columns) => {
    if (err) {
      return res.status(500).json({ error: 'Error fetching table columns' });
    }

    // Format the columns as a JSON array
    const columnInfo = columns.map(column => ({
      name: column.name,
      type: column.type,
    }));

    // Step 2: Fetch rows and their slots
    await db.all(selectTableSQL, (err, rows) => {
      if (err) {
        return res.status(500).json({ error: 'Error fetching table rows' });
      }

      // Format the rows and slots as a JSON object
      const rowData = {};
      rows.forEach(row => {
        rowData[row.id] = {
          name: row.name,
          value: row.value,
        };
      });

      // Send the JSON response
      res.json({ columns: columnInfo, rows: rowData });
    });
  });
});
*/
module.exports = router;

